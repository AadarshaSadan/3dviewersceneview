package com.ayata.roomwithsyncdata;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.widget.Toast;

import com.google.ar.core.exceptions.CameraNotAvailableException;
import com.google.ar.sceneform.HitTestResult;
import com.google.ar.sceneform.Node;
import com.google.ar.sceneform.Scene;
import com.google.ar.sceneform.SceneView;
import com.google.ar.sceneform.assets.RenderableSource;
import com.google.ar.sceneform.math.Vector3;
import com.google.ar.sceneform.rendering.ModelRenderable;
import com.google.ar.sceneform.rendering.Renderable;
import com.google.ar.sceneform.ux.FootprintSelectionVisualizer;
import com.google.ar.sceneform.ux.TransformationSystem;

import node.DragTransformableNode;

public class MainActivity extends AppCompatActivity {

    SceneView duckRenderable;
    private SceneView mSceneView;
    private Scene scene;
    private ModelRenderable model;
Context context;

   String localModel = "BagTextured.sfb";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mSceneView = findViewById(R.id.sceneView);
      //  scene = mSceneView.getScene();

//final String GLTF_ASSET = "https://github.com/KhronosGroup/glTF-Sample-Models/raw/master/2.0/Duck/glTF/Duck.gltf";

        /* When you build a Renderable, Sceneform loads model and related resources
         * in the background while returning a CompletableFuture.
         * Call thenAccept(), handle(), or check isDone() before calling get().
         */
        createScene();

    }

    private void createScene() {
        ModelRenderable.builder()
                .setSource(this, Uri.parse(localModel))
                .setRegistryId(localModel)
                .build()
                .thenAccept(modelRenderable -> onRenderableLoaded(modelRenderable))
                .exceptionally(throwable -> {
                    Toast toast =
                            Toast.makeText(this, "Unable to load model", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                    return null;
                });
    }

    void onRenderableLoaded(ModelRenderable model) {


        if(mSceneView!=null)
        {
//            Node modelNode = new Node();
//            modelNode.setRenderable(model);
//            modelNode.setParent(mSceneView.getScene());
//            modelNode.setLocalPosition(new Vector3(0, -0.1f, -1));
//            modelNode.setLocalScale(new Vector3(2,2,2));
//            mSceneView.getScene().addChild(modelNode);

            TransformationSystem transformationSystem=makeTransformationSystem();
            DragTransformableNode dragTransformableNode=new DragTransformableNode(1f,transformationSystem);

            if(dragTransformableNode!=null)
            {
                dragTransformableNode.setRenderable(model);
                mSceneView.getScene().addChild(dragTransformableNode);

             //   mSceneView.getScene().aa
                dragTransformableNode.select();
                mSceneView.getScene().addOnPeekTouchListener(new Scene.OnPeekTouchListener() {
                    @Override
                    public void onPeekTouch(HitTestResult hitTestResult, MotionEvent motionEvent) {
                        Log.d("touch",motionEvent.toString());

                        try {
                            transformationSystem.onTouch(hitTestResult,motionEvent);
                        }catch (Exception e)
                        {
                            e.printStackTrace();
                        }


                    }
                });
//                mSceneView.getScene().setOnTouchListener(new Scene.OnTouchListener() {
//                    @Override
//                    public boolean onSceneTouch(HitTestResult hitTestResult, MotionEvent motionEvent) {
//                      //  transformationSystem.onTouch(hitTestResult,motionEvent);
//                        return false;
//                    }});h




            }








        }


    }

    private TransformationSystem makeTransformationSystem(){

        FootprintSelectionVisualizer footprintSelectionVisualizer=new FootprintSelectionVisualizer();
        return new TransformationSystem(getResources().getDisplayMetrics(),footprintSelectionVisualizer);
    }


    @Override
    protected void onResume() {
        super.onResume();
        try {
            mSceneView.resume();
        } catch (CameraNotAvailableException e) {
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        mSceneView.pause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            mSceneView.destroy();
        }catch (Exception e)
        {
            e.printStackTrace();
        }

    }

}